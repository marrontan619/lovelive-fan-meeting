(function () {
  "use strict";
  var input1 = document.createElement("button"),
      input2 = document.createElement("button");
  input1.textContent = "入力1";
  input1.type = "button";
  input1.style.position = "fixed";
  input1.style.left = "1%";
  input1.style.bottom = "1%";
  input1.addEventListener("click", function () {
    chrome.storage.local.get(null, function (items) {
      document.getElementById("rdoSeatKindCd1").click();
      document.getElementById("selTktCnt").value = "2";
      document.getElementsByName("rdoKessaiMethod")[items["rdoKessaiMethod"]].click();
      document.getElementById("txtTelNo").value = items["txtTelNo"];
      document.getElementById("txtMailadd1").value = items["txtMailadd1"];
      document.getElementById("txtMailadd2").value = items["txtMailadd1"];
    });
  });
  input2.textContent = "入力2";
  input2.type = "button";
  input2.style.position = "fixed";
  input2.style.left = "10%";
  input2.style.bottom = "1%";
  input2.addEventListener("click", function () {
    chrome.storage.local.get(null, function (items) {
      document.getElementById("txtNmKanjjS").value = items["txtNmKanjjS"];
      document.getElementById("txtNmKanjjM").value = items["txtNmKanjjM"];
      document.getElementById("txtNmKanaS").value = items["txtNmKanaS"];
      document.getElementById("txtNmKanaM").value = items["txtNmKanaM"];
      document.getElementsByName("rdoSex")[items["rdoSex"]].click();
      document.getElementById("selYear").value = items["selYear"];
      document.getElementById("selMonth").value = items["selMonth"];
      document.getElementById("selDate").value = items["selDate"];
      document.getElementById("selAddrArea").value = items["selPrefecture"];
      document.getElementById("txtZipcodeUpper").value = items["txtZipcodeUpper"];
      document.getElementById("txtZipcodeLower").value = items["txtZipcodeLower"];
      document.getElementById("selPrefecture").value = items["selPrefecture"];
      document.getElementById("txtAddCity").value = items["txtAddCity"];
      document.getElementById("txtAddTown").value = items["txtAddTown"];
      document.getElementById("txtRoomNum").value = items["txtRoomNum"];
      (document.getElementById("selCardKind") || {}).value = items["selCardKind"];
      (document.getElementById("txtCardNum") || {}).value = items["txtCardNum"];
      (document.getElementById("selVldPeriodMonth") || {}).value = items["selVldPeriodMonth"];
      (document.getElementById("selVldPeriodYear") || {}).value = items["selVldPeriodYear"];
      document.getElementById("txtPassWord1").value = items["txtPassWord"];
      document.getElementById("txtPassWord2").value = items["txtPassWord"];
    });
  });
  document.body.appendChild(input1);
  document.body.appendChild(input2);
}).call(this);
