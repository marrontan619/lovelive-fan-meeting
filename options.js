addEventListener("DOMContentLoaded", function () {
  "use strict";
  chrome.storage.local.get(null, function (items) {
    var key;
    for (key in items) {
      document.getElementById(key).value = items[key];
    }
  });
  document.getElementById("save").addEventListener("click", function () {
    var inputs = document.querySelectorAll("input, select"),
        i = 0,
        l = inputs.length,
        items = {};
    for (; i < l; i++) {
      items[inputs[i].id] = inputs[i].value;
    }
    chrome.storage.local.set(items);
  });
});
